import React, { useEffect, useState, useContext } from "react";
import Field from "../Field/Field";
import { UtilisateurContext } from "../Login/Login";
import { AuthenticationContext } from "../AuthenticationContext/AuthenticationContext";
import {  useNavigate } from "react-router-dom";
import { LoginContext } from "../LoginContext/LoginContext";

interface Element {
  id: number;
  attributes: {
    libelle: string;
    etat: boolean;
  };
}

function Liste() {
  const [libelle, setLibelle] = useState("");
  const [etat, setEtat] = useState(false);
  const [formAjout, setFormAjout] = useState(false);
  const { authenticationToken } = useContext(AuthenticationContext);
  const [elements, setElements] = useState<Element[]>([]);
  const navigate = useNavigate();
  const { isLogin, setIsLogin } = useContext(LoginContext);
  const { userId, setUserId } = useContext(UtilisateurContext);
  console.log(authenticationToken)

  function fetchElements() {
    fetch(`http://localhost:1337/api/elements/?filters[user]=${userId}`, {
      method: "GET",
      headers: {
        "Authorization" : "Bearer " + authenticationToken
      }})
      .then(response => response.json())
      .then(data => setElements(data.data))
      .catch(error => console.log(error));
  }

  useEffect(() => {
    fetchElements();
  }, [userId]);

  function submitForm(e: any) {
    e.preventDefault();
    fetch("http://localhost:1337/api/elements", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization" : "Bearer " + authenticationToken
      },
      body: JSON.stringify({
        "data": {
          libelle: libelle,
          etat: etat,
          user : userId
        }
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data) {
          setFormAjout(false);
          fetchElements();
        } else {
          setFormAjout(true)
        }
      });
  }

  async function logout() {
    setUserId(0);
    setIsLogin(!isLogin)
    navigate('/');
  }

  function deleteElement(e: any) {
    const id = e.target.id;
    fetch(`http://localhost:1337/api/elements/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        "Authorization" : "Bearer " + authenticationToken
      }
    })
      .then(response => response.json())
      .then(data => {
        if (data) {
          fetchElements();
        }
      });
  }

  return (
    <div className="App">
      <div className="mt-0 justify-center bg-gray-100 py-12">
        <button className="border-solid border-2 border-sky-500 bg-sky-500 p-1 mt-8 mb-1 rounded-xl font-mono hover:bg-sky-300" onClick={() => setFormAjout(!formAjout)}>ajouter</button>
        <button className="border-solid border-2 border-red-500  ml-72 bg-red-500 p-1 mx-2 mt-8 mb-1 rounded-xl font-mono hover:bg-red-300" onClick={logout}>Se déconnecter</button>
        <form onSubmit={submitForm} className="formAjout" style={(formAjout === false) ? { visibility: "hidden" } : { visibility: "visible" }}>
          <Field className="border-solid border-2 border-zinc-300 pt-1 pb-1 rounded-xl font-mono" type="text" name="libelle" onChange={(e) => setLibelle(e.target.value)}>libelle :</Field>
          <Field className="border-solid border-2 border-zinc-300 pt-1 pb-1 rounded-xl font-mono" type="checkbox" name="etat" onChange={() => setEtat(!etat)}>{etat ? "fait" : "à faire"}</Field>
          <button className="border-solid border-2 border-green-500 bg-green-500 p-1 mt-8 rounded-xl font-mono hover:bg-green-400" type="submit">Valider</button>
        </form>
        {elements.map(element => (
          <div className="unElement" key={element.id}>
            <span className="libelle" style={(element.attributes.etat  === true) ? {color : "green"} :  {color : "red"}}>{element.attributes.libelle}  </span>
            <span className="etat">{(element.attributes.etat === true) ? " - fait " : " - à faire "}</span>
            <span><button className="border-solid border-2 border-red-500 bg-red-500 mt-8 rounded-md font-mono hover:bg-red-300 mx-2" onClick={deleteElement} id={element.id.toString()} value="supprimer">Supprimer</button></span>
          </div>
        ))}
        
      </div>
    </div>
  );
}

export default Liste;
