import React, { useContext, useState, createContext } from "react";
import { AuthenticationContext } from "../AuthenticationContext/AuthenticationContext";
import Field from "../Field/Field";
import { LoginContext } from "../LoginContext/LoginContext";
import { Link, useNavigate } from "react-router-dom";

export interface UtilisateurContext {
  userId: number,
  setUserId: (userId: number) => void
}
//export const MyGlobalContext = createContext<GlobalContent>
export const UtilisateurContext = createContext<UtilisateurContext>({
  userId: 0,
  setUserId: () => { },
})

export const UtilisateurProvider:  React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [userId, setUserId] = useState<number>(0);
  
  return (
  <UtilisateurContext.Provider value={{ userId, setUserId }}>
    {children}
  </UtilisateurContext.Provider>
);
}

function Login() {

  const { isLogin, setIsLogin } = useContext(LoginContext);
  const { setAuthenticationToken } = useContext(AuthenticationContext);
  const { setUserId } = useContext(UtilisateurContext);
  const [userName, setUserName] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const navigate = useNavigate();

  function submitForm(e: any) {
    e.preventDefault();
    fetch("http://localhost:1337/api/auth/local/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        identifier: userName,
        password: password
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data.jwt) {
          setAuthenticationToken(data.jwt);
          setIsLogin(true);
          setUserId(data.user.id)
          navigate('/liste')
        } else {
          setIsLogin(false);
        }
      });
  }

  return (
    <div className="App">
      <div className="justify-center bg-gray-100 py-12">
        <h1 className="text-lg" style={(isLogin === true) ? { visibility: "visible" } : { visibility: "hidden" }}>Welcome {userName} !</h1>
        <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900" style={isLogin ? { visibility: "hidden" } : { visibility: "visible" }}>Login</h2>
        <form className="mt-8" onSubmit={submitForm} style={isLogin ? { visibility: "hidden" } : { visibility: "visible" }}>
          <Field className="border-solid border-2 border-zinc-300 pt-1 pb-1 rounded-xl font-mono my-2" type="text" name="username" onChange={(e) => setUserName(e.target.value)}>Name user : </Field>
          <Field className="border-solid border-2 border-zinc-300 pt-1 pb-1 rounded-xl font-mono my-2" type="password" name="password" onChange={(e) => setPassword(e.target.value)}>Password : </Field>
          <button className="border-solid border-2 border-green-500 bg-green-500 p-1 mt-8 rounded-xl mx-4 font-mono hover:bg-green-400" type="submit">Valider</button>
         <Link to="/register"><button className="border-solid border-2 border-green-500 bg-green-500 p-1 mt-8 rounded-xl font-mono hover:bg-green-400">Inscription</button></Link>
        </form>
      </div>
    </div>
  );
}

export default Login;