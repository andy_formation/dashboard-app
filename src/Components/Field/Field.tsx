import React from "react";
interface FieldProps {
    className: string;
    type: string
    name: string
    value?: any
    children: string
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Field: React.FC<FieldProps> = ({ children, className, value, type, name, onChange }) => {
    return (
      <div>
        <label>{children}</label>
        <input className={className} type={type} value={value} name={name} onChange={onChange} />
      </div>
    )
}
export default Field;