import { createContext, useState } from "react";
import React from "react";

export interface AuthenticationContext {
  authenticationToken: string,
  setAuthenticationToken: (authenticationToken: string) => void
}

export const AuthenticationContext = createContext<AuthenticationContext>({
  authenticationToken: "",
  setAuthenticationToken: () => { },
})

export const AuthenticationProvider:  React.FC<{ children: React.ReactNode }> = ({ children }) => {
    const [authenticationToken, setAuthenticationToken] = useState("");

    return (
        <AuthenticationContext.Provider value={{ authenticationToken, setAuthenticationToken }}>
          {children}
        </AuthenticationContext.Provider>
    );
};
