import { createContext, useState } from "react";
import React from "react";

export interface LoginContext {
  isLogin: boolean,
  setIsLogin: (isLogin: boolean) => void
}

export const LoginContext = createContext<LoginContext>({
  isLogin: false,
  setIsLogin: () => { },
})
export const LoginProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [isLogin, setIsLogin] = useState<boolean>(false);

  return (
    <LoginContext.Provider value={{ isLogin, setIsLogin }}>
      {children}
    </LoginContext.Provider>
  );
  };