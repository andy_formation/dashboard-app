import React, { useState } from "react";
import Field from "../Field/Field";
import { useNavigate } from 'react-router-dom';

function Register() {

    const [email, setEmail] = useState<string>("");
    const [firstName, setFirstName] = useState<string>("");
    const [lastName, setLastName] = useState<string>("");
    const [userName, setUserName] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [message, setMessage] = useState<string>("");
    const navigate = useNavigate();

  function submitForm(e: any) {
    e.preventDefault();
    fetch("http://localhost:1337/api/auth/local/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        username: userName,
        email: email,
        password: password,
        firstName: firstName,
        lastName: lastName
      })
    }) 
    .then(response => response.json())
    .then(data => {
      if (data.jwt) {
        navigate('/');
      } else {
        setMessage("Une erreur est survenue !");
      }
    });
  }

  return (
    <div className="App">
      <div className="justify-center bg-gray-100 py-12">
        <form onSubmit={submitForm}>
          <Field className="border-solid border-2 border-zinc-300 pt-1 pb-1 rounded-xl font-mono my-2" type="text" name="username" onChange={(e) => setUserName(e.target.value)}>Name user : </Field>
          <Field className="border-solid border-2 border-zinc-300 pt-1 pb-1 rounded-xl font-mono my-2" type="email" name="email" onChange={(e) => setEmail(e.target.value)}>Email : </Field>
          <Field className="border-solid border-2 border-zinc-300 pt-1 pb-1 rounded-xl font-mono my-2" type="text" name="firstName" onChange={(e) => setFirstName(e.target.value)}>First Name : </Field>
          <Field className="border-solid border-2 border-zinc-300 pt-1 pb-1 rounded-xl font-mono my-2" type="text" name="lastName" onChange={(e) => setLastName(e.target.value)}>Last Name : </Field>
          <Field className="border-solid border-2 border-zinc-300 pt-1 pb-1 rounded-xl font-mono my-2" type="password" name="password" onChange={(e) => setPassword(e.target.value)}>Password : </Field>
          <button className="border-solid border-2 border-green-500 bg-green-500 p-1 mt-8 rounded-xl font-mono hover:bg-green-400" type="submit">Valider</button>
        </form>
        {message}
      </div>
    </div>
  );
}

export default Register;