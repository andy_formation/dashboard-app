import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Liste from './Liste/Liste';
import Login from './Login/Login';
import Register from './Register/Register';

function Routepath() {
  return (
        <Router>
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/liste" element={<Liste />} />
          </Routes>
        </Router>
  );
}

export default Routepath;
