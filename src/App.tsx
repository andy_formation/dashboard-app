import './App.css';
import { UtilisateurProvider } from './Components/Login/Login';
import { LoginProvider } from './Components/LoginContext/LoginContext';
import { AuthenticationProvider } from './Components/AuthenticationContext/AuthenticationContext';
import React from 'react';
import Routepath from './Components/Routepath';

function App() {
  return (
    <AuthenticationProvider>
      <UtilisateurProvider>
        <LoginProvider>
          <Routepath></Routepath>
        </LoginProvider>
      </UtilisateurProvider>
    </AuthenticationProvider>
  );
}

export default App;
